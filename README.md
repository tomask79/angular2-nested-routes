# Writting applications in Angular 2, [part 11] #

## Nested routes ##

Last time in [part 10](https://bitbucket.org/tomask79/angular2-basic-routing) I was talking about basic *introduction
of the routing types* in Angular 2. I showed you also **basic routing** with twitter bootstrap based menu and drawing 
components into router-outlet. If you know [EmberJS](http://emberjs.com/) then this should sounds familiar.
Basic routing was kind of changing the whole page, **but what if you want routing inside of the component (like tabs)**?
Then **nested routes** comes into the game (Yes, Angular team was also inspired by EmberJS here). Nested Routes
gives us what Misko Hevery calls "**Lazy Loading**". Only particular components are always shown with Nested Routes.

## Howto create Nested Routes in Angular 2 ##

Let's change product list component from the last demo **to simple TAB component with two links** which are going 
to be changing the content of the page based on the clicked tab. Perfect use case for nested routes. Our **RouterConfig**
is going to be:

```
export const routes: RouterConfig = [  
  { path: '', redirectTo: '/productList', pathMatch: 'full'},  
  { path: 'productList', component: ProductListComponent,
    children: [
      { path: '', component: ProductsComponent }, 
      { path: 'best-sellers', component: BestSellerProductsComponent } 
    ] 
  },
  { path: 'newProducts', component: ProductFormComponent }
];
```
By property **children** you're binding which components are going to be shown in the router-outlet tag of **ProductListComponent** after click at routerLink
with URL's '/productList' and '/productList/best-sellers'. What is not obvious from the documentation is the fact **that you need to have in children at least one path
with '' URL.** Why? Because if you visit just http://context:port/productList then Angular 2 won't know what to put inside of the router-outlet and you will end
with crash **'/productList' route not found**. Hence empty URL mapping is needed.

**Component ProductListComponent (our TAB component):**

```
import { Component } from '@angular/core';
import { ROUTER_DIRECTIVES } from '@angular/router';

@Component({
    selector: 'product-menu',
    template: `
        <a [routerLink]="['/productList']">All products</a>
        <a [routerLink]="['/productList/best-sellers']">Best Sellers</a>
        <router-outlet></router-outlet>
    `,
    directives: [ROUTER_DIRECTIVES]
})
export class ProductListComponent {
}
```
Again, important to understand here is to realize that configured routerLink components won't be shown in the parent's router-outlet, but in the ProductListComponent's router-outlet. Because they're configured as children. 

** Component ProductsComponent **

```
import {Component, OnInit} from '@angular/core';
import {ProductService} from './product.service';
import {ProductType} from './product';
import {ProductDetailComponent} from './product.detail.component';
import {ProductFormComponent} from './product-form.component';
import { provideRouter, RouterConfig } from '@angular/router';

@Component({
    selector: 'products',
    template: `     
              <ul>
                  <li *ngFor="let product of _products">
                     <product-detail [product]="product"></product-detail>
                  </li>
              </ul>       
    `,
    directives: [ProductDetailComponent]
})
export class ProductsComponent implements OnInit{
    protected _products: ProductType[];

    constructor(protected _productService: ProductService) {
    }

    public ngOnInit() {
        this._products = this.getProducts();
    }

    protected getProducts() : ProductType[] {
        return this._productService.getAllProducts();
    }
}
```
Nothing special here, this component shows "all products". But for best selling components we will need to call different 
ProductService's method. And there is nothing easier in typescript then just override this method in the new component...

** Component BestSellerProductsComponent **

```
import {ProductsComponent} from './products.component';
import {ProductType} from './product';

export class BestSellerProductsComponent extends ProductsComponent {

    // Override parent method for getting products.
    public getProducts() : ProductType[] {
        return this._productService.getBestSellerProductList();
    }
}
```
And that's it! Enjoy the nested routes in Angular 2. 

# Testing the demo #

as usual:

* git clone <this repo>
* npm install
* npm start
* visit localhost:3000

best regards

Tomas