"use strict";
var platform_browser_dynamic_1 = require('@angular/platform-browser-dynamic');
var app_component_1 = require('./app.component');
var app_router_1 = require('./app.router');
platform_browser_dynamic_1.bootstrap(app_component_1.AppComponent, [app_router_1.APP_ROUTER_PROVIDERS]);
//# sourceMappingURL=main.js.map