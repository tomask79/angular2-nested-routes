import { Component } from '@angular/core';
import { ROUTER_DIRECTIVES } from '@angular/router';

@Component({
    selector: 'product-menu',
    template: `
        <a [routerLink]="['/productList']">All products</a>
        <a [routerLink]="['/productList/best-sellers']">Best Sellers</a>
        <router-outlet></router-outlet>
    `,
    directives: [ROUTER_DIRECTIVES]
})
export class ProductListComponent {
}