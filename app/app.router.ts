import { provideRouter, RouterConfig } from '@angular/router';

import { ProductFormComponent } from './product-form.component';
import { ProductsComponent } from './products.component';
import { BestSellerProductsComponent } from './best-seller.products.component';
import { ProductListComponent} from './product.list.component';
import { AppComponent } from './app.component';

export const routes: RouterConfig = [  
  { path: '', redirectTo: '/productList', pathMatch: 'full'},  
  { path: 'productList', component: ProductListComponent,
    children: [
      { path: '', component: ProductsComponent }, 
      { path: 'best-sellers', component: BestSellerProductsComponent } 
    ] 
  },
  { path: 'newProducts', component: ProductFormComponent }
];

export const APP_ROUTER_PROVIDERS = [
  provideRouter(routes)
];