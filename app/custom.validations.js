"use strict";
var CustomValidators = (function () {
    function CustomValidators() {
    }
    CustomValidators.priceValueValidation = function (control) {
        // Validation is OK only if control is not changed or has number bigger > 0 otherwise throw error.
        return (control.pristine || parseInt(control.value)) > 0 ? null : { "lessThenZeroPriceError": true };
    };
    return CustomValidators;
}());
exports.CustomValidators = CustomValidators;
//# sourceMappingURL=custom.validations.js.map