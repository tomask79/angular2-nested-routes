"use strict";
var router_1 = require('@angular/router');
var product_form_component_1 = require('./product-form.component');
var products_component_1 = require('./products.component');
var best_seller_products_component_1 = require('./best-seller.products.component');
var product_list_component_1 = require('./product.list.component');
exports.routes = [
    { path: '', redirectTo: '/productList', pathMatch: 'full' },
    { path: 'productList', component: product_list_component_1.ProductListComponent,
        children: [
            { path: '', component: products_component_1.ProductsComponent },
            { path: 'best-sellers', component: best_seller_products_component_1.BestSellerProductsComponent }
        ]
    },
    { path: 'newProducts', component: product_form_component_1.ProductFormComponent }
];
exports.APP_ROUTER_PROVIDERS = [
    router_1.provideRouter(exports.routes)
];
//# sourceMappingURL=app.router.js.map