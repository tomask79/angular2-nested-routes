import {ProductsComponent} from './products.component';
import {ProductType} from './product';

export class BestSellerProductsComponent extends ProductsComponent {

    // Override parent method for getting products.
    public getProducts() : ProductType[] {
        return this._productService.getBestSellerProductList();
    }
}